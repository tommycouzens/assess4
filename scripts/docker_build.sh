#!/bin/bash

# copy the jar file
aws s3 cp s3://mints3/spring-petclinic-2.0.0.jar docker/petclinic/files/spring-petclinic-2.0.0.jar

cd docker/petclinic

$(aws ecr get-login --no-include-email --region us-east-1)



docker build -t mint .

# get the uri of the repo
uri=$(aws ecr describe-repositories --region us-east-1 --repository-names mint \
| jq -r '.[][].repositoryUri')


docker tag mint:latest ${uri}:latest
docker push ${uri}:latest
