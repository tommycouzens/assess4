#!/bin/bash

cd docker

export DBCENDPOINT=$(aws rds describe-db-clusters --region us-east-1 --db-cluster-identifier \
aurora-cluster-mint | jq -r '.DBClusters[].Endpoint')

# Get the DB username and password from the vault
TOKEN=$(cat /var/lib/jenkins/token)
IP=$(cat /var/lib/jenkins/petclinicip)
export DBUSER=$(curl -v -sk -X GET -H "X-Vault-Token:$TOKEN" https://$IP:8200/v1/secret/admin | jq -r '.data.value' )
export DBPASS=$(curl -v -sk -X GET -H "X-Vault-Token:$TOKEN" https://$IP:8200/v1/secret/password | jq -r '.data.value')


aws s3 cp s3://mints3/spring-petclinic-2.0.0.jar petclinic/files/spring-petclinic-2.0.0.jar

ls petclinic/files


# remove existing containers
docker rm -f $(docker ps -a | awk '{print $1}')

# remove existing images
docker rmi -f $(docker images | awk {'print $3'})



docker-compose up -d


sleep 60

# check if veterinarians is part of the page
echo "Curling for veterinarians"
if ! curl -s localhost:3080 | grep veterinarians
then
  exit 1
fi


docker-compose down

# remove existing containers
docker rm -f $(docker ps -a | awk '{print $1}')

# remove existing images
docker rmi -f $(docker images | awk {'print $3'})
