#Grab the database cluster endpoint

if cd petclinic
then
  git pull
else
  git clone https://JangleFett@bitbucket.org/JangleFett/petclinic.git
  cd petclinic
fi


# First create the properties file
./mkprops $1 $2 $3 $4

# Debug, check that config file has changed
cat src/main/resources/application.properties

# Now perform maven build
export M2_HOME=/usr/local/apache-maven-3.6.0
export M2=$M2_HOME/bin
export PATH=$M2:$PATH
source /etc/profile
mvn -Dmaven.test.skip=true package


# Copy jar file to s3 bucket
aws s3 cp target/spring-petclinic-2.0.0.jar  s3://mints3/spring-petclinic-2.0.0.jar
