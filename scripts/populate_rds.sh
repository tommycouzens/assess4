#!/bin/bash


sudo yum -y install git

if cd assess3
then
  git pull
else
  git clone https://tommycouzens@bitbucket.org/tommycouzens/assess3.git
  cd assess3
fi


cd petclinic/src/main/resources/db/mysql
# mysql -h $DBCEndpoint -u ${DBUSER} -p${DBPASS} < schema.sql
# mysql -h $DBCEndpoint -u ${DBUSER} -p${DBPASS} < data.sql

mysql -h $1 -u $2 -p${3} < schema.sql
mysql -h $1 -u $2 -p${3}  < data.sql
