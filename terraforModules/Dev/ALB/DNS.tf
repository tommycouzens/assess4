data "aws_route53_zone" "selected" {
 name         = "grads.al-labs.co.uk."
 private_zone = false
}
#gives our webapp a DNS name www.MiNT.grads.al-labs.co.uk
resource "aws_route53_record" "www" {
 zone_id = "${data.aws_route53_zone.selected.zone_id}"
 name    = "www.MiNT.grads.al-labs.co.uk"
 type    = "A"
 alias {
    name                   = "${aws_alb.main.dns_name}"
    zone_id                = "${aws_alb.main.zone_id}"
    evaluate_target_health = true
  }
}
