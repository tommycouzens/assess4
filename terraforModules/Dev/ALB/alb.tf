#creates load balancer with autoscaling ability
resource "aws_alb" "main" {
  name            = "mintLB"
  subnets         = ["${var.pubSubs}"]
  security_groups = ["${var.ecsLbSecurityId}"]
}

resource "aws_alb_target_group" "app" {
  name        = "mintAPP"
  port        = "${var.app_port}"
  protocol    = "HTTP"
  vpc_id      = "${var.vpcId}"
  target_type = "ip"
}

# Redirect all traffic from the ALB to the target group
resource "aws_alb_listener" "front_end" {
  load_balancer_arn = "${aws_alb.main.id}"
  port              = "${var.app_port}"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.app.id}"
    type             = "forward"
  }
}
