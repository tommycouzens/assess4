#creates ecr repository
resource "aws_ecr_repository" "default" {
  name = "${var.repository_name}"
}
#collects data from ecr repository
data "aws_ecr_repository" "default" {
  name = "${var.repository_name}"
}
