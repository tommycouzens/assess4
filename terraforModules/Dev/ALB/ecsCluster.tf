#ECSCluster
resource "aws_ecs_cluster" "main" {
  name = "tf-ecs-cluster"
}

#collects data from aws_iam_role
data "aws_iam_role" "ecs_task_execution_role" {
  name = "ecsTaskExecutionRole"
}
#policy for ecs repository
resource "aws_iam_policy" "access_policy" {
  name = "accessPolicyASG"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
    {
        "Effect": "Allow",
        "Action": [
            "ecr:GetAuthorizationToken",
            "ecr:BatchCheckLayerAvailability",
            "ecr:GetDownloadUrlForLayer",
            "ecr:BatchGetImage",
            "logs:CreateLogStream",
            "logs:PutLogEvents"
            ],
        "Resource": "*"
        }
    ]
}
EOF
}
#role for ecs repository
resource "aws_iam_role" "github-role" {
  name = "accessRoleASG"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
            "Service": [
                "s3.amazonaws.com",
                "lambda.amazonaws.com",
                "ecs.amazonaws.com",
                "ecs-tasks.amazonaws.com"
            ]
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}
#attched iam role to policy
resource "aws_iam_role_policy_attachment" "test-attach" {
    role       = "${aws_iam_role.github-role.name}"
    policy_arn = "${aws_iam_policy.access_policy.arn}"
}

##### THERE WAS A BUG WITH TERRAFORM SO COULD NOT CREATE RESOURCE aws_ecs_task_definition BELOW #####
#### created aws_ecs_task_definition manually on aws website ####

#defines task for containers
# resource "aws_ecs_task_definition" "app" {
#   family                   = "appp"
#   network_mode             = "awsvpc"
#   requires_compatibilities = ["FARGATE"]
#   cpu                      = "${var.fargate_cpu}"
#   memory                   = "${var.fargate_memory}"
#   task_role_arn            = "${aws_iam_role.github-role.arn}"
#   execution_role_arn       = "${data.aws_iam_role.ecs_task_execution_role.arn}"
#
#   container_definitions = <<DEFINITION
# [
#   {
#     "cpu": "${var.fargate_cpu}",
#     "image": "${data.aws_ecr_repository.default.repository_url}",
#     "memory": "${var.fargate_memory}",
#     "name": "MiNTdockerImage",
#     "networkMode": "awsvpc",
#     "portMappings": [
#       {
#       "containerPort": "${var.app_port}",
#         "hostPort": "${var.app_port}"
#       }
#     ]
#   }
# ]
# DEFINITION
# }

#Provides an ECS service - effectively a task that is expected to run until an error occurs or a user terminates it. Also, sets count for containers.
resource "aws_ecs_service" "main" {
  name            = "mint_tf_ecs_service"
  cluster         = "${aws_ecs_cluster.main.id}"
  task_definition = "app",           #"${aws_ecs_task_definition.app.arn}"
  desired_count   = "${var.app_count}"
  launch_type     = "FARGATE"

  network_configuration {
    security_groups = ["${aws_security_group.ecs_tasks.id}"]
    subnets         = ["${aws_subnet.private.*.id}"]
  }

  load_balancer {
    target_group_arn = "${aws_alb_target_group.app.id}"
    container_name   = "MiNTdockerImage"
    container_port   = "${var.app_port}"
  }

  depends_on = [
    "aws_alb_listener.front_end",
  ]
}
