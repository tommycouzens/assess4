output "arn" {
  value = "${aws_ecr_repository.default.arn}"
}

output "name" {
  value = "${aws_ecr_repository.default.name}"
}

output "registry_id" {
  value = "${aws_ecr_repository.default.registry_id}"
}

output "repository_url" {
  value = "${aws_ecr_repository.default.repository_url}"
}
output "alb_hostname" {
  value = "${aws_alb.main.dns_name}"
}
# output "roleAlbArn" {
#   value="${aws_iam_role.ecs_autoscale_role.arn}"
# }
