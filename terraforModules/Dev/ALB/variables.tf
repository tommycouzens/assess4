variable "repository_name" {
  description = "ECR Repository name"
}

variable "fargate_cpu" {
  description = "Fargate instance CPU units to provision (1 vCPU = 1024 CPU units)"
}

variable "fargate_memory" {
  description = "Fargate instance memory to provision (in MiB)"
}

variable "app_port" {
  description = "Port exposed by the docker image to redirect traffic to"
}

variable "app_count" {
  description = "Number of docker containers to run"
}

variable "privSubs" {
  type = "list"
  description = "Private subnet ids"
}
variable "pubSubs" {
  type = "list"
  description = "Public subnet ids"
}
variable "vpcId" {
  description = "VPC id"
}
variable "ecsSecurityId" {
  description = "Private subnet ids"
}
variable "ecsLbSecurityId" {
  description = "security group id for the ecs ALB"
}

variable "vpcSecId" {
  description = "Public subnet ids"
}
# variable "roleAlbArn" {
#   description = "Public subnet ids"
# }
