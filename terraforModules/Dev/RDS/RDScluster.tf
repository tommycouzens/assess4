#Manages a RDS Aurora Cluster.
resource "aws_rds_cluster" "default" {
  cluster_identifier = "aurora-cluster-mint"
  db_subnet_group_name = "${var.dbSubnetGroupName}"
  # availability_zones = ["us-east-1a", "us-east-1b"]
  availability_zones   =  ["${var.AZs}"]
  database_name      = "mydb"
  master_username    = "${var.username_mysql}"
  master_password    = "${var.password_mysql}"
  skip_final_snapshot = "true"
  vpc_security_group_ids = ["${var.vpcSecId}"]

}

resource "aws_rds_cluster_instance" "cluster_instances" {
  count              = 2
  identifier         = "aurora-cluster-mint-${count.index}"
  cluster_identifier = "${aws_rds_cluster.default.id}"
  instance_class     = "db.t2.small"
  db_subnet_group_name = "${var.dbSubnetGroupName}"
}
