#an ec2 instance in the public subnet so that you can ssh onto RDS database
resource "aws_instance" "bastian" {
  ami           = "${lookup(var.amis, var.aws_region)}"
  subnet_id = "${element(var.pubSubs, 0)}"
  instance_type = "t2.micro"
  key_name = "${var.key_name}"
  vpc_security_group_ids = ["${var.vpcSecIdssh}"]
  tags {
    Name = "MiNTbastion"
  }
}
