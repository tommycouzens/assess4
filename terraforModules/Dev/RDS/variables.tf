variable "amis" {
 type = "map"
}

variable "aws_region" {
  description = "The AWS region to create things in."
}

variable "key_name" {
  description = "Key name for NV"
}

variable "AZs" {
  description = "Availability zones"
  type = "list"
}
variable "username_mysql" {
  description = "Username for MYSQL DB."
}
variable "password_mysql" {
  description = "Password for MYSQL DB."
}
variable "vpcSecId" {
  description = "Public subnet ids"
}
variable "vpcSecIdssh" {
  description = "Security group to enable MySQL access from within the VPC"
}
variable "pubSubs" {
  type = "list"
  description = "Public subnet ids"
}
variable "privSubs" {
  type = "list"
  description = "Private subnet ids"
}
variable "dbSubnetGroupName" {
  description = "Private subnet ids"
}
