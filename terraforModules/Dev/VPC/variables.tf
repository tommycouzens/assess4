variable "az_count" {
  description = "Number of AZs to cover in a given AWS region"
}
variable "app_port" {
  description = "Port exposed by the docker image to redirect traffic to"
}

# variable "vpcSecIdssh" {
#   description = "Security group to enable MySQL access from within the VPC"
# }
