module "VPC" {
  source="./VPC"
  app_port="80"
  az_count="2"

}

module "RDS" {
  source="./RDS"
  username_mysql="Mint"
  password_mysql="freshmint3"
  AZs=["us-east-1a", "us-east-1b", "us-east-1c"]
  key_name="mintyy"
  amis= {"eu-west-2" = "ami-0274e11dced17bb5b", "us-east-1" = "ami-009d6802948d06e52"}
  aws_region="us-east-1"
  vpcSecIdssh="${module.VPC.vpcSecIdssh}"
  vpcSecId="${module.VPC.vpcSecId}"
  privSubs="${module.VPC.privSubs}"
  pubSubs="${module.VPC.pubSubs}"
  dbSubnetGroupName="${module.VPC.dbSubnetGroupName}"
}

module "ALB" {
  source="./ALB"
  app_port="80"
  app_count="2"
  fargate_cpu="256"
  fargate_memory="512"
  repository_name="mint"
  #az_count="2"
  vpcId="${module.VPC.vpcId}"
  ecsSecurityId="${module.VPC.ecsSecurityId}"
  ecsLbSecurityId="${module.VPC.ecsLbSecurityId}"
  privSubs="${module.VPC.privSubs}"
  pubSubs="${module.VPC.pubSubs}"
  vpcSecId="${module.VPC.vpcSecId}"
  #roleAlbArn="${module.ALB.roleAlbArn}"

}
