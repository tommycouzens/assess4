# Fetch AZs in the current region
data "aws_availability_zones" "available" {}

#creates VPC
resource "aws_vpc" "main" {
  cidr_block = "172.17.0.0/16"
  enable_dns_hostnames = "true"
  tags {
    Name = "MiNTvpc"
  }
}

# Create var.az_count private subnets, each in a different AZ
resource "aws_subnet" "private" {
  count             = "${var.az_count}"
  cidr_block        = "${cidrsubnet(aws_vpc.main.cidr_block, 8, count.index)}"
  availability_zone = "${data.aws_availability_zones.available.names[count.index]}"
  vpc_id            = "${aws_vpc.main.id}"
  tags {
    Name = "MiNTprivSubnet"
  }
}

# Create var.az_count public subnets, each in a different AZ
resource "aws_subnet" "public" {
  count                   = "${var.az_count}"
  cidr_block              = "${cidrsubnet(aws_vpc.main.cidr_block, 8, var.az_count + count.index)}"
  availability_zone       = "${data.aws_availability_zones.available.names[count.index]}"
  vpc_id                  = "${aws_vpc.main.id}"
  map_public_ip_on_launch = true
  tags {
    Name = "MiNTpubSubnet"
  }
}

output "vpcId" {
  value = "${aws_vpc.main.id}"
}
output "privSubs" {
  value = "${aws_subnet.private.*.id}"
}
output "pubSubs" {
  value = "${aws_subnet.public.*.id}"
}
