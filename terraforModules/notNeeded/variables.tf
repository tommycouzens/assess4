variable "aws_region" {
  description = "The AWS region to create things in."
  default     = "us-east-1"
}

variable "AZs" {
  description = "Availability zones"
  type = "list"
  default = ["us-east-1a", "us-east-1b", "us-east-1c"]
}

variable "amis" {
 type = "map"
 default = {
   "eu-west-2" = "ami-0274e11dced17bb5b"
   "us-east-1" = "ami-009d6802948d06e52"
 }
}

variable "aws_account_id" {
  description = "AWS account ID"
  default = "miya.pniel"
}

variable "app_port" {
  description = "Port exposed by the docker image to redirect traffic to"
  default     = 80
}

variable "app_count" {
  description = "Number of docker containers to run"
  default     = 2
}

variable "fargate_cpu" {
  description = "Fargate instance CPU units to provision (1 vCPU = 1024 CPU units)"
  default     = "256"
}

variable "fargate_memory" {
  description = "Fargate instance memory to provision (in MiB)"
  default     = "512"
}

variable "username_mysql" {
  description = "Username for MYSQL DB."
  default     = "Mint"
}

variable "password_mysql" {
  description = "Password for MYSQL DB."
  default     = "freshmint3"
}

variable "key_name" {
  description = "Key name for NV"
  default     = "MiNT"
}

variable "repository_name" {
  description = "ECR Repository name"
  default     = "mint"
}

variable "attach_lifecycle_policy" {
  default = true
}

variable "lifecycle_policy" {
  default = ""
}

variable "name" {
  default = "ecr"
}

variable "az_count" {
  description = "Number of AZs to cover in a given AWS region"
  default     = "2"
}
