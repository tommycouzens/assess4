resource "aws_instance" "bastian" {
  ami           = "${lookup(var.amis, var.aws_region)}"
  subnet_id = "${element(aws_subnet.public.*.id, 0)}"
  instance_type = "t2.micro"
  key_name = "${var.key_name}"
  vpc_security_group_ids = ["${aws_security_group.world_SSH.id}"]
  user_data = "${file("./bastionapps.sh")}"

  tags {
    Name = "MiNTbastiAn"
  }
}
