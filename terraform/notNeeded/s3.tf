resource "aws_s3_bucket" "bucket" {
  bucket = "mints3"
  acl    = "private"
  tags {
    Name        = "MiNTs3"
    #Environment = "Dev"
  }
}
