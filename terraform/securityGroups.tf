#RDS Security Group
resource "aws_security_group" "rds" {
  name        = "rds_group"
  description = "Enable rds access from within the VPC"
  vpc_id      = "${aws_vpc.main.id}"

  ingress {
    protocol    = "tcp"
    from_port   = 3306
    to_port     = 3306
    cidr_blocks = ["${aws_vpc.main.cidr_block}"]
  }

  tags {
    Name = "MiNTSqlSecGroup"
  }
}

resource "aws_security_group" "world_SSH" {
  name        = "world_ssh"
  description = "Enable MySQL access from within the VPC"
  vpc_id      = "${aws_vpc.main.id}"

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["77.108.144.180/32"]

  }
  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "MiNTworldSSH"
  }
}
