variable "aws_region" {
  description = "The AWS region to create things in."
  default     = "us-east-1"
}

variable "AZs" {
  description = "Availability zones"
  type = "list"
  default = ["us-east-1a", "us-east-1b"]
}

variable "amis" {
  type = "map"
  default = {
    "eu-west-2" = "ami-0274e11dced17bb5b"
    "us-east-1" = "ami-009d6802948d06e52"
  }
}

variable "aws_account_id" {
  description = "AWS account ID"
  default = "miya.pniel"
}

variable "az_count" {
  description = "Number of AZs to cover in a given AWS region"
  default     = "2"
}

variable "app_port" {
  description = "Port exposed by the docker image to redirect traffic to"
  default     = 8080
}

variable "app_count" {
  description = "Number of docker containers to run"
  default     = 2
}

variable "fargate_cpu" {
  description = "Fargate instance CPU units to provision (1 vCPU = 1024 CPU units)"
  default     = "256"
}

variable "fargate_memory" {
  description = "Fargate instance memory to provision (in MiB)"
  default     = "512"
}

variable "username_mysql" {
  description = "Username for MYSQL DB."
  default     = "Mint"
}

variable "password_mysql" {
  description = "Password for MYSQL DB."
  default     = "freshmint3"
}

variable "key_name" {
  description = "Key name for NV"
  default     = "mintyy"
}

variable "repository_name" {
  description = "ECR Repository name"
  default     = "mint"
}

variable "attach_lifecycle_policy" {
  default = true
}

variable "lifecycle_policy" {
  default = ""
}

variable "name" {
  default = "ecr"
}

variable "autoscaling_policy_arn" {
  default = "arn:aws:iam::aws:policy/aws-service-role/AWSApplicationAutoscalingECSServicePolicy"
}

variable "lb_port" {
  default = "8080"
}

# variable "pubSubs" {
#   type = "list"
#   description = "Public subnet ids"
#   default = "${aws_subnet.public.*.id}"
# }
# variable "privSubs" {
#   type = "list"
#   description = "Private subnet ids"
#   default ="${aws_subnet.private.*.id}"
# }

# variable "schedule_expression" {
#   default = "cron(5 * * * ? *)"
#   description = "the aws cloudwatch event rule scheule expression that specifies when the scheduler runs. Default is 5 minuts past the hour. for debugging use 'rate(5 minutes)'. See https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html"
# }
#
# variable "tag" {
#   default = "schedule"
#   description = "the tag name used on the EC2 or RDS instance to contain the schedule json string for the instance."
# }
#
# variable "schedule_tag_force" {
#   type = "string"
#   default = "false"
#   description = "Whether to force the EC2 or RDS instance to have the default schedule tag is no schedule tag exists for the instance."
# }
#
# variable "exclude" {
#   default = ""
#   description = "common separated list of EC2 and RDS instance ids to exclude from scheduling."
# }
#
# variable "default" {
#   default = "{\"mon\": {\"start\": 8, \"stop\": 20},\"tue\": {\"start\": 8, \"stop\": 20},\"wed\": {\"start\": 8, \"stop\": 20},\"thu\": {\"start\": 8, \"stop\": 20}, \"fri\": {\"start\": 8, \"stop\": 20}}"
#   description = "the default schedule tag containing json schedule information to add to instance when schedule_tag_force set to true."
# }
#
# variable "time" {
#   default = "gmt"
#   description = "timezone to use for scheduler. Can be 'local' or 'gmt'. Default is gmt. local time is for the AWS region."
# }
#
# variable "ec2_schedule" {
#   type = "string"
#   default = "true"
#   description = "Whether to do scheduling for EC2 instances."
# }
#
# variable "rds_schedule" {
#   type = "string"
#   default = "true"
#   description = "Whether to do scheduling for RDS instances."
# }
#
# variable "pubSubs" {
#   type = "list"
#   description = "Public subnet ids"
#   default = ""
# }

# variable "security_group_ids" {
#   type = "list"
#   description = "Public subnet ids"
#   default = "${aws_security_group.world_SSH.id}"
# }

# variable "security_group_ids" {
#   type = "list"
#   default = []
#   description = "list of the vpc security groups to run lambda scheduler in."
# }

# variable "subnet_ids" {
#   type = "list"
#   default = []
#   description = "list of subnet_ids that the scheduler runs in."
# }
